package com.mygdx.game;
import java.util.Date;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

public class Napisy {

	public BitmapFont font24;
	//private Date nowaData;
	private FreeTypeFontGenerator generatorTekstwGrze; //generator 1
	//private FreeTypeFontGenerator generatorTytu�Menu; ////generator 2
	public BitmapFont BitmapFontGracz1, BitmapFontGracz2, BitmapNews;
	private String stringNapisGracz1, stringNapisGracz2, NapisNews;
	int zycie;
	
	public Napisy()
	{
		//nowaData = new Date();
		generatorTekstwGrze = new FreeTypeFontGenerator(Gdx.files.internal("czcionka2.ttf")); //generator
		FreeTypeFontGenerator.FreeTypeFontParameter paramsTextwGrze = new FreeTypeFontGenerator.FreeTypeFontParameter();
		//generatorTytu�Menu = new FreeTypeFontGenerator(Gdx.files.internal("czcionka2.ttf"));
		//FreeTypeFontGenerator.FreeTypeFontParameter paramsTekstMenu = new FreeTypeFontGenerator.FreeTypeFontParameter();
		paramsTextwGrze.size = 30;
		paramsTextwGrze.color = Color.NAVY;
		//paramsTekstMenu.size = 30;
		//paramsTekstMenu.color = Color.SKY;
		//BitmapFont_Tytu�Menu = generatorTytu�Menu.generateFont(paramsTytu�Menu); 
		BitmapFontGracz1 = generatorTekstwGrze.generateFont(paramsTextwGrze); 
		BitmapFontGracz2 = generatorTekstwGrze.generateFont(paramsTextwGrze); 
		stringNapisGracz1 = "100 HP";
		stringNapisGracz2 = "100 HP";
		zycie = 100;
		
		FreeTypeFontGenerator.FreeTypeFontParameter paramsNews = new FreeTypeFontGenerator.FreeTypeFontParameter();
		paramsNews.size = 50;
		paramsNews.color = Color.MAROON;
		BitmapNews = generatorTekstwGrze.generateFont(paramsNews); 
		
	}
	public void setStrataGracz2(int obrazenia)
	{
		this.zycie -= obrazenia;
		this.stringNapisGracz2=""+this.zycie+" HP";
	}
	public void setMartwy2()
	{
		this.stringNapisGracz2="                DEAD";
	}
	
	public int getZycie()						{return this.zycie;}
	
	public BitmapFont get_BitmapFontGracz1()	{return this.BitmapFontGracz1;}  			//get Bitmap
	public BitmapFont get_BitmapFontGracz2()	{return this.BitmapFontGracz2;}
	public BitmapFont get_BitmapFontNews()  	{return this.BitmapNews;}
	
	public String get_stringNapisGracz1()		{return this.stringNapisGracz1;}			//get String
	public String get_stringNapisGracz2()   	{return this.stringNapisGracz2;}
	
	
	
	
	
}
