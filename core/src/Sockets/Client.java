package Sockets;
import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Net.Protocol;
import com.badlogic.gdx.net.Socket;
import com.badlogic.gdx.net.SocketHints;
import com.badlogic.gdx.utils.Disposable;


public class Client implements Disposable  {
	public Socket socket;
	private ClientThread thread;
	private boolean trwanie;
	public String XY;
	private String kawalekXY;
	public float X, Y;
	public SocketHints hints;
	public byte[] read = new byte[1024];
	public String readString;

	public Client()
	{
		thread = new ClientThread();
		thread.start();
		trwanie = true;
		XY = "";
	}
	public void WylaczPentle()
	{
		this.trwanie = false;
	}
	public void StringToFloat()
	{
			int PozycjaSzukanegoSlasza = XY.indexOf("/");
			if(PozycjaSzukanegoSlasza != -1)
			{
				kawalekXY = XY.substring(0,PozycjaSzukanegoSlasza);
				X = Float.parseFloat(   kawalekXY  );
				kawalekXY = XY.substring(PozycjaSzukanegoSlasza+1);
				this.Y = Float.parseFloat(   kawalekXY  );
			}
			else
			{

			}
	}
	public float getX()
	{
		return X;
	}
	
	public float getY()
	{
		return Y;
	}
	
	
	@Override
	public void dispose() {
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(socket != null)
			socket.dispose();
		
	}
	public class ClientThread extends Thread{
		
		public void run(){
			
			hints = new SocketHints();
			hints.connectTimeout = 11000;
			try {
					socket = Gdx.net.newClientSocket(Protocol.TCP, "localhost", 8783, hints );
				   Thread.sleep(500, 0);
				   } catch (Exception e) {
				   System.out.println(e);
				   }
			while(trwanie)
			{
				if(socket!=null)
				{
					try {
						socket.getOutputStream().write(new String("CZESC server").getBytes()); // wiadomosc wysylana read.length
						socket.getInputStream().read(read, 0, read.length); //odebrana od servera
						readString = new String(read).trim();
						XY = readString;
						StringToFloat();
						
					} catch (IOException e) {
						e.printStackTrace();socket.dispose();
					}
				}
				if(Gdx.input.isKeyPressed(Keys.ESCAPE))
				{
					 WylaczPentle();
					 System.out.println("Wychodzimy z pentli");
					 Gdx.app.exit();
				}
			}
			socket.dispose();		
		}
	}

}
