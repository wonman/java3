package com.mygdx.game;

import Sockets.*;
import News.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Date;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.net.Socket;

public class MyGdxGame extends ApplicationAdapter {
	SpriteBatch batch;
	private Texture   tMapa2,dtLudek1,dtLudek2,  tLudek1,tLudek2, tMenu, tOautorze, tInfo, tWypis, tBronAK47, tSala;
	private Texture tKula, tMsql, tMusicBox, granat, wybuch, tChmura, tPorazka, tTloGwiezdne, tLudekMartwy2, tHelp, tNews, tPodkladkaNews;
	private GameObject gracz1, gracz2;
	private OrthographicCamera camera;
	private Game NowaGra;
	private RuchPostaci NowyRuch;
	private Menu NoweMenu;
	private Bronie bronieKula, bronieGranat;
	int viewportHeight;
	Date nowaData;
	private Music NowMusic;
	private float jumpTime, Timer_gry, shootTime,shootTimeAccept, botTime, granatTime, Timer_HttpManager;
	FreeTypeFontGenerator generator;
	BitmapFont  bounds;
	boolean obrot;
	float Timer_person, Timer_MusicBox,Timer2_MusicBox;
	private Napisy NowyNapis;
	private Driver NowyDriver;
	private MusicBox NowaMusicBox;
	private Music musicHeroic, musicSad, musicWOSP;
	float granatStareX, granatStareY, wybuchX, wybuchY, granatStartTime;
	ScreenShoot nowyScreenShoot;
	thread NowyThread;
	int ABC=1;
	float ChmuraX=0, ChmuraY=-450;
	float ChmuraTime;	
	float wskaznikX = (float) 0.0002;
	float wskaznikY = (float) 0.0002;
	boolean ChmuraRun = true;
	boolean BotZycie = true;
	public HTTP_Manager nowyHttpManager;
	//public Tvn24 nowyTvn241;
	LiveNews nowyLiveNews;
	PobierzNews nowyPobierzNews;
	
	private final int IS_SERVER = 0;								//sockets  0 - single, 1 - serv, 2 - client	
	private Server server;
	private Client client;
	public float x,y;
	public boolean zmianaMapy = false;
	
	@Override
	public void create () {	
		obrot = false;
		Gdx.audio.newMusic(Gdx.files.internal("Music_main.mp3"));
		
		camera = new OrthographicCamera(850, 550);
		batch = new SpriteBatch();
		Gdx.audio.newMusic(Gdx.files.internal("musicMenu.mp3"));
		musicHeroic  = Gdx.audio.newMusic(Gdx.files.internal("musicHeroic.mp3"));
		musicSad   = Gdx.audio.newMusic(Gdx.files.internal("musicSad.mp3"));
		musicWOSP  = Gdx.audio.newMusic(Gdx.files.internal("musicWOSP.mp3"));
		tMapa2 = new Texture("mapa2.png");	
		dtLudek1 = new Texture("dludek1.png");
		dtLudek2 = new Texture("dludek2.png");
		tMenu = new Texture ("menu.png");
		tOautorze = new Texture("oautorze.png");
		tInfo = new Texture("info.png");
		tWypis = new Texture("wypis.png");
		tMsql = new Texture("msql.png");
		tBronAK47 = new Texture("ak47.png");
		tKula = new Texture("kula.png");
		tMusicBox = new Texture("musicbox.png");
		granat = new Texture("granat.png");
		wybuch = new Texture("wybuch.png");
		tChmura = new Texture("chmura.png");
		tPorazka = new Texture("porazka.png");
		tTloGwiezdne = new Texture("CzarneTlo.png");
		tLudekMartwy2 = new Texture("dludek2Martwy.png");
		tHelp = new Texture("dsasd.png");
		tNews = new Texture("News.png");
		bronieKula = new Bronie(tKula, 550, 120); ///
		bronieGranat = new Bronie(granat, 550, 120);
		Timer_gry = 0;
		NowaMusicBox = new MusicBox();
		//font = new BitmapFont();
		NowaGra = new Game();
		NowyRuch = new RuchPostaci();
		NowyDriver = new Driver();
		gracz1 = new GameObject(dtLudek1, 550, 120);
		nowyHttpManager = new HTTP_Manager();
		nowaData = new Date();
		gracz2 = new GameObject(dtLudek2, 600, 120);
		NoweMenu = new Menu();
		NowyNapis = new Napisy();
		obrot = false;
		nowyScreenShoot = new ScreenShoot();
		NowyThread = new thread();
		//nowyTvn241 = new Tvn24();
		nowyLiveNews = new LiveNews();
		tPodkladkaNews = new Texture("podkladkaNews.png");
		nowyPobierzNews = new PobierzNews();
		tSala = new Texture("sala.png");
		
		
		
		if(IS_SERVER == 1)
		{
			server = new Server();
			System.out.println("@@Server@@@@@@@");
		}
		if(IS_SERVER == 2)
		{
			client = new Client();
			System.out.println("##Client########");
		}
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 1, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game();
	
	}
	
	@SuppressWarnings("deprecation")
	public void update()
	{
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		camera.position.set(gracz1.x + gracz1.width/2+100, gracz1.y + gracz1.height/2+250, 0);
		
		if(Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))
		{
			camera.zoom+=0.02;
			//System.out.println("zoom: "+camera.zoom);
		}
		if(Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT))
			camera.zoom-=0.02;
		
		jumpTime += Gdx.graphics.getDeltaTime();
		if( (Gdx.input.isKeyPressed(Keys.W) && jumpTime>0.1) || NowyRuch.getWarunekSkoku()  ) //gora
		{
			NowyRuch.setTrue();
			float skok = -25*jumpTime * jumpTime + 25* jumpTime;
			gracz1.y += skok;
			if(gracz1.y<120)
			{
				NowyRuch.setFalse();
				gracz1.y=120;
				jumpTime =0;
			}
			if(IS_SERVER == 1)
				server.konwersjaX(gracz1.y);
		}
		if(gracz1.x>2500)
		{
			zmianaMapy= true;
			System.out.println("WUWUWUW");
		}
		
		granatTime+= Gdx.graphics.getDeltaTime();
		if(Gdx.input.isKeyPressed(Keys.Q)  || bronieGranat.granatWarunek == true)  //granat mechanika //&& granatStartTime
				{
			
					if(granatTime > 0.03)
						{
						granatTime=0;
						bronieGranat.granatRzut();
						if((gracz2.x > ( gracz1.x +20 + bronieGranat.GetXGranat() ) &&  gracz2.x <  ( gracz1.x +70 + bronieGranat.GetXGranat() ) )) //zabranie HP
						{
							NowyNapis.setStrataGracz2(25);
							if(NowyNapis.getZycie() <= 0)
							{
								gracz2 = new GameObject(tLudekMartwy2, gracz2.x, gracz2.y);
								NowyNapis.setMartwy2();
								BotZycie= false;
							}
						}
						}
					if(Gdx.input.isKeyPressed(Keys.Q))
					bronieGranat.granatSetTrue();
					
				}
		Timer_MusicBox += Gdx.graphics.getDeltaTime();						//MusicBox    
		if(Gdx.input.isKeyPressed(Keys.N ) && Timer_MusicBox>0.3)
		{
			System.out.println("aktualna -> : "+NowaMusicBox.aktualnaMusic);
			if(NowaMusicBox.aktualnaMusic== 1)
				NowMusic.stop();
			else if(NowaMusicBox.aktualnaMusic== 2)
				NowMusic.stop();
			else if(NowaMusicBox.aktualnaMusic== 3)
				NowMusic.stop();
			System.out.println("aktualna zaraz po -> : "+NowaMusicBox.aktualnaMusic);
			
			int warunek = NowaMusicBox.zmianaMusic();
			
			if(warunek== 1)
				NowMusic = musicHeroic;
			else if(warunek== 2)
				NowMusic = musicSad;
			else if(warunek== 3)
				NowMusic = musicWOSP;
			
			NowMusic.play();
			Timer_MusicBox = 0;
		}
		if(Gdx.input.isKeyPressed(Keys.F1 ) && Timer_MusicBox>0.1) //nowyHttpManager
		{
			nowyScreenShoot.screen();
			//NowyDriver.insertUser("AWP", 15);
			//NowyDriver.insertUser(8, "AWP", 15);
			//NowyDriver.upddateKarabin("GS53","Snajpa",11);
			NowyDriver.createTabele("Explosions", "nazwa", "TEXT", "obrazenia", "INT");
		}
		if(Gdx.input.isKeyPressed(Keys.F2 ) && Timer_MusicBox>0.4)
		{
			ABC++;
			System.out.println("Tworzymy w�tek");
			
			Thread t = new Thread(new Runnable() 
			{ public void run() {
				
				while(ChmuraRun)
				{
					ChmuraX += wskaznikX;
					ChmuraY += wskaznikY;
					
					if(ChmuraX > 20)
					{
						wskaznikX *=(-1);
						ChmuraX = 19;
					}
					if(ChmuraX < -20)
					{
						wskaznikX *=(-1);
						ChmuraX = -19;
					}
					if(ChmuraY >0)
					{
						wskaznikY =0;
						//ChmuraX = -19;
						ChmuraRun = false;
					}
					try {
						Thread.sleep((long) 0.99);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					 System.out.println("");
				}
			}
			});
			t.start();
		}
		Timer_HttpManager += Gdx.graphics.getDeltaTime();	
		if(Gdx.input.isKeyPressed(Keys.F3 ) && Timer_HttpManager>0.3) 							//nowyHttpManager
		{
			//nowyHttpManager.OtworzStrone();																							///////////////////WWW
			Timer_HttpManager = 0;
			callRuby();
			System.out.println("RUB");
			
		}
		Timer2_MusicBox += Gdx.graphics.getDeltaTime();	
		if(Gdx.input.isKeyPressed(Keys.M ) && Timer2_MusicBox>0.3)
		{
			System.out.println("cisza? : "+NowaMusicBox.aktualnaMusic);
			if(NowaMusicBox.aktualnaMusic== 1)
				NowMusic.stop();
			else if(NowaMusicBox.aktualnaMusic== 2)
				NowMusic.stop();
			else if(NowaMusicBox.aktualnaMusic== 3)
				NowMusic.stop();

			Timer2_MusicBox = 0;
		}
		if(Gdx.input.isKeyPressed(Keys.A)&& gracz1.x>600) //lewo ruch	
		{
			gracz1.x -=10;
			if(IS_SERVER == 1)
			server.konwersjaX(gracz1.x);
		}
		
		if(Gdx.input.isKeyPressed(Keys.D)&& gracz1.x<2900) //prawo ruch
		{
			gracz1.x +=10;
			if(IS_SERVER == 1)
			server.konwersjaX(gracz1.x);
		}
		
		shootTimeAccept += Gdx.graphics.getDeltaTime();
		shootTime += Gdx.graphics.getDeltaTime();
		if( Gdx.input.isKeyPressed(Keys.CONTROL_LEFT) && shootTimeAccept>1.5  ) //strzal
		{
			bronieKula.strza�Accept();
			bronieKula.setShotTrue();
			shootTimeAccept = 0;
		}
		if(bronieKula.getShot() == true && shootTime > 0.2)
		{
			bronieKula.strza�();
			if((gracz2.x > ( gracz1.x +20 + bronieKula.strza�() ) &&  gracz2.x <  ( gracz1.x +70 + bronieKula.strza�()) ) && !obrot ) //zabranie HP
			{
				NowyNapis.setStrataGracz2(10);
				if(NowyNapis.getZycie() < 0)
				{
					gracz2 = new GameObject(tLudekMartwy2, gracz2.x, gracz2.y);
					NowyNapis.setMartwy2();
					BotZycie= false;
				}	
			}
			if((gracz2.x > ( gracz1.x +20 - bronieKula.strza�() ) &&  gracz2.x <  ( gracz1.x +70 - bronieKula.strza�()) ) && obrot ) //zabranie HP
			{
				
				NowyNapis.setStrataGracz2(10);
				if(NowyNapis.getZycie() <= 0)
				{
					gracz2 = new GameObject(tLudekMartwy2, gracz2.x, gracz2.y);
					NowyNapis.setMartwy2();
					BotZycie= false;
				}	
			}
		}
		
		botTime += Gdx.graphics.getDeltaTime();
		if(IS_SERVER == 2)
		{
			gracz2.x = client.getX();
			gracz2.y = client.getY();
		}

		camera.update(); //podwojnie kamera upd
	}
	public void draw()
	{
		batch.draw(tMapa2, 0, 0);
		
		Timer_person+= Gdx.graphics.getDeltaTime();
		if(Gdx.input.isKeyPressed(Keys.E)&&Timer_person> 0.2 )
		{
		  if(obrot == false)
			  obrot = true;
		  else
			  obrot = false;
		  Timer_person = 0;
		}
		batch.draw(gracz1.getTexture(), gracz1.x, gracz1.y, 155, 325, 0, 0,  155, 325, obrot, false); //gracz 1 ruch 
		batch.draw(tBronAK47, gracz1.x, gracz1.y+140, 191, 61, 0, 0,  191, 61, obrot, false); // bron AK47 ruch
		batch.draw(gracz2.getTexture(), gracz2.x, gracz2.y); // gracz 2 ruch
		batch.draw(granat, gracz1.x + bronieGranat.granatX, gracz1.y+150+ bronieGranat.granatY); // granat granatTime
		batch.draw(tMusicBox, gracz1.x-460, gracz1.y+520); //MusicBox
		
		if(bronieGranat.granatWarunek == true)
		{
			batch.draw(granat, granatStareX + bronieGranat.granatX, 150 + bronieGranat.granatY+ granatStareY); // aktywny granat
			wybuchX =granatStareX + bronieGranat.granatX;
			wybuchY =150 + bronieGranat.granatY+ granatStareY - 100;
			
		}
		
		else if(bronieGranat.granatWybuch>0)
		{
			batch.draw(wybuch, wybuchX, wybuchY); // wybuch granat
			bronieGranat.granatWybuch--;
		}
		else
		{
			granatStareX=gracz1.x; 
			granatStareY=gracz1.y; 
		}
		
		Timer_gry += Gdx.graphics.getDeltaTime();
		String napisik = Float.toString(Timer_gry); 
		NowyNapis.get_BitmapFontGracz1().draw(batch, NowyNapis.get_stringNapisGracz1() ,gracz1.x+45, gracz1.y+370); //gracz1 napis nad g�ow�
		NowyNapis.get_BitmapFontGracz2().draw(batch, NowyNapis.get_stringNapisGracz2(),gracz2.x+45, gracz2.y+370);  //gracz2 napis nad g�ow�
		NowyNapis.get_BitmapFontGracz1().draw(batch, "TIME   "+napisik, gracz1.x+400, gracz1.y+570); 				//napis czasu
		if(Gdx.input.isKeyPressed(Keys.J) )
		{
			batch.draw(tPodkladkaNews,gracz1.x-245, 10);																				//NEWS 	
			NowyNapis.get_BitmapFontNews().draw(batch, nowyHttpManager.getMeteoOdczuwalna()+" stopni !!! "+nowyLiveNews.getLiveNews(),gracz1.x+nowyLiveNews.przewijanie()+900, 80); 					
			batch.draw(tNews, gracz1.x-500, 0); 
			
		}
		
		
		
		if(bronieKula.getShot())
		{
			if(obrot == false)
			batch.draw(tKula, gracz1.x +50 + bronieKula.strza�(), gracz1.y+185); 
			if(obrot == true)
			batch.draw(tKula, gracz1.x +50 + bronieKula.strza�()*(-1), gracz1.y+185); 
		}
		batch.draw(tChmura, ChmuraX, ChmuraY);
		
		if(Gdx.input.isKeyPressed(Keys.H))
		{
			
				batch.draw(tHelp, gracz1.x-465,0+gracz1.y-140);
		}
		if(zmianaMapy)
		{
			tMapa2 = new Texture("sala.png");
			gracz1.x = 2000;
			zmianaMapy  = false;
		}
		
	}
	
    public static void callRuby(){

        ScriptEngine jruby = new ScriptEngineManager().getEngineByName("jruby");
          //process a ruby file
          try {           
               jruby.eval(new BufferedReader(new FileReader("cos.rb")));
               
               //jruby.put("a", "2");
               //jruby.put("b", "3"); 
               //jruby.eval("$a = 2");
               //jruby.eval("$b = 3");
               jruby.eval("addition()");
               //System.out.println("result: " + jruby.get($res) );
               
               //jruby.eval("addition()");
               
               //System.out.println("result: " +jruby.get("res"));
               
               
               
               

          } catch (FileNotFoundException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
          } catch (ScriptException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
          }

  }
	
	public void game()
	{
		if(NowaGra.getStanGry() == -1) //zapis daty do pliku
		{
			NoweMenu.Opcja0();
			NowaGra.setStanGry(0);
			
		}
		batch.begin();
		Gdx.gl.glClearColor(1, 1, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);	// czyszczenie
		
		if(NowaGra.getStanGry() == 0) //nie gra nikt
		{
			camera.zoom= 1f;
			batch.setProjectionMatrix(camera.combined);
			camera.position.set(425, 275, 0);
			camera.update();
			batch.draw(tMenu, 0, 0);
			NowaGra.setStanGry( NoweMenu.Opcja1() );
			if(Gdx.input.isKeyPressed(Keys.TAB)) Gdx.app.exit(); //wyjscie TAB
		}
		
		else if(NowaGra.getStanGry() == 1) //preGra
		{
			camera.zoom= 1.33f;
			NowaGra.setStanGry( 2 );
			gracz2 = new GameObject(dtLudek2, gracz2.x, gracz2.y);
			BotZycie = true;
		}
		else if(NowaGra.getStanGry() == 2)
		{
			draw();
			update();
			NowaGra.setStanGry( NoweMenu.ESC(2) );
			if(ChmuraY >=0)
				//NoweMenu.Porazka();
				NowaGra.setStanGry(7);
			
		}
		else if(NowaGra.getStanGry() == 3) // wybrano info
		{
			batch.draw(tInfo, 0, 0);
			NowaGra.setStanGry( NoweMenu.ESC(3) );
		}
		else if(NowaGra.getStanGry() == 4) // wybrano oautorach
		{
			batch.draw(tOautorze, 0, 0);		
			NowaGra.setStanGry( NoweMenu.ESC(4) );
			//if()
		}
		else if(NowaGra.getStanGry() == 5) // wizyty
		{
			batch.draw(tWypis, 0, 0);
			FileHandle file = Gdx.files.internal("data.txt");
			String text = file.readString();
			file.exists();
			NoweMenu.get_BitmapFont_TekstMenu().draw(batch, text , 177, 380); 
			NoweMenu.get_BitmapFont_Tytu�Menu().draw(batch, NoweMenu.get_String_Tytu�Hightscores() , 140, 500); 
					
			NowaGra.setStanGry( NoweMenu.ESC(5) );
			if(Gdx.input.isKeyPressed(Keys.BACKSPACE)) //usuwani wpis�w
			{
				FileHandle file1 = Gdx.files.local("data.txt");
				file1.writeString("" , false);
				System.out.println("reset");
			}
		}
		else if(NowaGra.getStanGry() == 6) // MySql
		{
			batch.draw(tMsql, 0, 0);	
			String tmp = NowyDriver.getKarabin();
			String tmp2 = NowyDriver.getPistolet();
			String tmp3 = NowyDriver.getN�();
			
			NoweMenu.get_String_Mysql().draw(batch, tmp , 40, 500); //wypis karabin
			NoweMenu.get_String_Mysql().draw(batch, tmp2 , 325, 500); //wypis pistolet
			NoweMenu.get_String_Mysql().draw(batch, tmp3 , 600, 500); //wypis n�
			
			NowaGra.setStanGry( NoweMenu.ESC(6) );
		}
		else if(NowaGra.getStanGry() == 7) // Porazka
		{	camera.zoom= 1f;
			batch.setProjectionMatrix(camera.combined);
			camera.position.set(425, 275, 0);
			camera.update();
			batch.draw(tTloGwiezdne, 0,0);	
			batch.draw(tPorazka, 50, 180);	
			ChmuraY = -480;
			wskaznikY = (float) 0.0002;	
			ChmuraRun = true;
			
			
			NowaGra.setStanGry( NoweMenu.ESC(7) );
		}
		batch.end(); //--
	}
	
	
	@Override
	public void dispose() 
	{
		System.out.print("wyj�cie");
		super.dispose(); 
		if(IS_SERVER != 0)
		{

			if(IS_SERVER == 1)
			{
				server.dispose();
			}
			if(IS_SERVER == 2)
			{
				client.dispose();
			}
		}
	}
}