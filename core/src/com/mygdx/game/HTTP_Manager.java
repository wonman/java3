package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.HttpMethods;
import com.badlogic.gdx.Net.HttpRequest;
import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.net.HttpRequestBuilder;

public class HTTP_Manager extends ApplicationAdapter implements HttpResponseListener {

	private int meteoOdczuwalna, meteoMaxymalna,meteoWiatr;
	public  HTTP_Manager()
	{
		HttpRequestBuilder requestBuilder = new HttpRequestBuilder();	// http://tvnmeteo.tvn24.pl/pogoda/krakow,17135/na-dzis-na-jutro,1.html
		HttpRequest request = requestBuilder.newRequest().method(HttpMethods.GET).url("http://tvnmeteo.tvn24.pl/pogoda/krakow,17135/na-dzis-na-jutro,1.html").content("q=ibgdx&example=example").build(); //http://tvnmeteo.tvn24.pl/pogoda/krakow,17135/na-dzis-na-jutro,1.html
		Gdx.net.sendHttpRequest(request,this);
	}

	@Override
	public void failed(Throwable t) {
		System.out.println("Brak po��czenia z internetem pogoda");
	}

	@Override
	public void cancelled() {
		System.out.println("Zatrzymane po��czenia z internetem");
	}

	@Override
	public void handleHttpResponse(HttpResponse httpResponse) {
	
		//System.out.println(new String(httpResponse.getResult()));
		String CalaStrona = httpResponse.getResultAsString();
		//System.out.println(nowy1);
		
		FileHandle file = Gdx.files.local("pogoda.txt");
		
		file.writeString(CalaStrona , true);
		file.writeString(" ".replaceAll("\\s+",System.getProperty("line.separator")), true);
		file.exists();
		
		
		int numer = CalaStrona.indexOf("Najwyższa temperatura</span><span>");  		//35 length		file:///C:/Users/Wojtek/Desktop/pogoda.html
		String MaxTemperatura = CalaStrona.substring(numer+35, numer+37);
		System.out.println("Najwyzsza temperatura:  "+MaxTemperatura);
		
		numer = CalaStrona.indexOf("Temperatura odczuwalna</span><span>"); 				//35 length
		String OdczuwalnaTemperatura = CalaStrona.substring(numer+35, numer+37);
		System.out.println("Odczuwalna temperatura:  "+OdczuwalnaTemperatura);			
		
		numer = CalaStrona.indexOf("<span>Wiatr</span><span>"); 						//24 length
		String Wiatr = CalaStrona.substring(numer+24, numer+26);
		System.out.println("Wiatr temperatura:  "+Wiatr);			
		
		
		this.meteoOdczuwalna =  Integer.parseInt(MaxTemperatura);
		this.meteoMaxymalna  =  Integer.parseInt(OdczuwalnaTemperatura);
		this.meteoWiatr      =  Integer.parseInt(Wiatr);
		
	}
	public int getMeteoOdczuwalna() {return this.meteoOdczuwalna;}
	public int getmeteoMaxymalna() {return this.meteoMaxymalna;}
	public int getmeteoWiatr() {return this.meteoWiatr;}
	
	public void OtworzStrone()
	{
		Gdx.net.openURI("file:///C:/Users/Wojtek/Desktop/IS_Java.html");
	}
}
