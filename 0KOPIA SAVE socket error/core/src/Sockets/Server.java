package Sockets;

import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Net.Protocol;
import com.badlogic.gdx.net.ServerSocket;
import com.badlogic.gdx.net.ServerSocketHints;
import com.badlogic.gdx.net.Socket;
import com.badlogic.gdx.net.SocketHints;
import com.badlogic.gdx.utils.Disposable;

public class Server implements Disposable {
	private ServerSocket server;
	private ServerThread thread;
	private boolean trwanie; 
	public String WspolzednaX, WspolzednaY;
	public ServerSocketHints hints;
	public SocketHints socketHints;
	public Socket socket;
	public byte[] read = new byte[1024];
	
	public Server()
	{
		thread = new ServerThread();
		thread.start();
		trwanie = true;
		WspolzednaX = "0";
		WspolzednaY = "100";
	}
	
	public void konwersjaX(float X)
	{
		int tmp = (int)X;
		this.WspolzednaX = ""+tmp;	// X
	}
	public void konwersjaY(float Y)
	{
		int tmp = (int)Y;
		this.WspolzednaY = ""+tmp; // Y
	}

	@Override
	public void dispose() {
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		server.dispose();
	}
	public void WylaczPentle()
	{
		this.trwanie = false;
	}
	public void setTcpNoDelay(boolean on)
	{
		//on = false;
		//TCP_NODELAY = on;
	}
	public class ServerThread extends Thread{
		
		public void run()
		{
			//setTcpNoDelay(false);
			hints = new ServerSocketHints();
			hints.acceptTimeout = 12000;
			
			
			try {
				socketHints = new SocketHints();
				server = Gdx.net.newServerSocket(Protocol.TCP, "localhost", 8784, hints);	
				socket  = server.accept(socketHints);
				   Thread.sleep(500, 0);
				   } catch (Exception e) {
				   System.out.println(e);
				   }
			
			while(trwanie)
			{
					//socketHints = new SocketHints();
					//server = Gdx.net.newServerSocket(Protocol.TCP, "localhost", 8783, hints);	
					//socket  = server.accept(socketHints);

				if(socket != null)
				{
				
					//byte[] read = new byte[1024];
					try {
						socket.getInputStream().read(read, 0, read.length);
						socket.getOutputStream().write( (WspolzednaX+"/"+WspolzednaY).getBytes());																			//---
						
					} catch (IOException e) {
						e.printStackTrace();
						//server.dispose();
					}
				}
				server.dispose();
				if(Gdx.input.isKeyPressed(Keys.ESCAPE))
				{
					 WylaczPentle();
					 System.out.println("Wychodzimy z pentli");
					 Gdx.app.exit();
				}
			}
			server.dispose();	
		}	
	}
}
