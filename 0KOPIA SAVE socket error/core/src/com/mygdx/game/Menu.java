package com.mygdx.game;

import java.util.Date;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType.Bitmap;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import javafx.scene.chart.PieChart.Data;

public class Menu {
	public BitmapFont font24;
	private Date nowaData;
	private FreeTypeFontGenerator generatorTekstMenu; //generator 1
	private FreeTypeFontGenerator generatorTytu�Menu; ////generator 2
	private BitmapFont BitmapFont_Tytu�Menu;
	private BitmapFont BitmapFont_Mysql;
	private BitmapFont BitmapFont_TekstMenu;
	private String String_Tytu�Hightscores;
	private String String_TextHightscores;
	
	public Menu()
	{
		nowaData = new Date();
		generatorTekstMenu = new FreeTypeFontGenerator(Gdx.files.internal("czcionka2.ttf")); //generator
		FreeTypeFontGenerator.FreeTypeFontParameter paramsTytu�Menu = new FreeTypeFontGenerator.FreeTypeFontParameter();
		generatorTytu�Menu = new FreeTypeFontGenerator(Gdx.files.internal("czcionka2.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter paramsTekstMenu = new FreeTypeFontGenerator.FreeTypeFontParameter();
		FreeTypeFontGenerator.FreeTypeFontParameter paramsMysql = new FreeTypeFontGenerator.FreeTypeFontParameter();    //mysql wypisy
		paramsTytu�Menu.size = 100;
		paramsTytu�Menu.color = Color.NAVY;
		paramsTekstMenu.size = 30;
		paramsTekstMenu.color = Color.SKY;
		paramsMysql.size = 30;
		paramsMysql.color = Color.GOLD;
		BitmapFont_Tytu�Menu = generatorTytu�Menu.generateFont(paramsTytu�Menu); 
		BitmapFont_Mysql = generatorTytu�Menu.generateFont(paramsMysql); 
		BitmapFont_TekstMenu = generatorTekstMenu.generateFont(paramsTekstMenu); 
		String_Tytu�Hightscores = "HIGHSCORES";

	}
	
	public BitmapFont get_BitmapFont_TekstMenu()
	{
		return this.BitmapFont_TekstMenu;
	}
	public BitmapFont get_BitmapFont_Tytu�Menu()
	{
		return this.BitmapFont_Tytu�Menu;
	}
	public String get_String_Tytu�Hightscores()
	{
		return this.String_Tytu�Hightscores;
	}
	public BitmapFont get_String_Mysql()
	{
		return this.BitmapFont_Mysql;  //sql
	}
	
	public void Opcja0()
	{
		FileHandle file = Gdx.files.local("data.txt");
		file.writeString(nowaData.toString() , true);
		file.writeString(" ".replaceAll("\\s+",System.getProperty("line.separator")), true);
		System.out.println("zapis");
		file.exists();
		//generator.dispose(); // don't forget to dispose to avoid memory leaks!
		
	}
	
	public void dispose()
	{
		font24.dispose();
		//Date.dispose();
		System.out.println("menu kosz");
	}
	public int Opcja1()
	{
		int result=0;
		if(Gdx.input.isKeyPressed(Keys.NUM_1)) 
			result= 1;
		if(Gdx.input.isKeyPressed(Keys.NUM_2)) 
			result= 3;
		if(Gdx.input.isKeyPressed(Keys.NUM_3)) 
			result= 4;
		if(Gdx.input.isKeyPressed(Keys.NUM_4)) 
			Gdx.app.exit();
		if(Gdx.input.isKeyPressed(Keys.NUM_5)) 
			result= 5;
		if(Gdx.input.isKeyPressed(Keys.NUM_6)) 
			result= 6;
		return result;
	}
	public int Porazka()
	{
		return 7;
	}
	public int ESC(int wynik)
	{
		if(Gdx.input.isKeyPressed(Keys.ESCAPE))
		{
			//camera.update();
			//NowaGra.setStanGry(0);
			wynik = 0;
		}
		return wynik;
	}
		

}
