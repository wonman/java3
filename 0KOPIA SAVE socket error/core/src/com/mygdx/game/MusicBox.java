package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;

public class MusicBox {

	private Music musicMenu, musicHeroic, musicSad, musicWOSP;
	public int aktualnaMusic;
	public float timer;
	public void MusicBox()
	{
		
		musicMenu  = Gdx.audio.newMusic(Gdx.files.internal("musicMenu.mp3"));
		musicHeroic  = Gdx.audio.newMusic(Gdx.files.internal("musicHeroic.mp3"));
		musicSad   = Gdx.audio.newMusic(Gdx.files.internal("musicSad.mp3"));
		musicWOSP  = Gdx.audio.newMusic(Gdx.files.internal("musicWOSP.mp3"));
		aktualnaMusic = 0;
	}
	public void playMusic(String nazwa)
	{
		if(nazwa == "musicMenu" ) 
		{
			this.musicMenu.play();
			this.aktualnaMusic = 1;
		}
		if(nazwa == "musicHeroic") 
		{
			this.musicHeroic.play();
			this.aktualnaMusic = 2;
		}
		if(nazwa == "musicSad")  
		{
			this.musicSad.play();
			this.aktualnaMusic = 3;
		}
		if(nazwa == "musicWOSP") 
		{
			this.musicWOSP.play();
			this.aktualnaMusic = 4;
		}
		
	}
	public Music przelaczMusic()
	{
		Music wynik = null;
		if(this.aktualnaMusic == 1 ) //aktualnaMusic = 1
		{
			System.out.println("sss"+this.aktualnaMusic);
			wynik = musicMenu;
			//musicMenu.play();
		}
		if(this.aktualnaMusic == 2) //aktualnaMusic = 2
		{
			
			wynik = musicHeroic;
		}
		if(this.aktualnaMusic == 3)  //aktualnaMusic = 3
		{
			wynik = musicSad;
		}
		if(this.aktualnaMusic == 4) //aktualnaMusic = 4
		{
			wynik = musicWOSP;
		}
		return wynik;
		
	}
	public void wylaczMusic()
	{

		if(this.aktualnaMusic == 1) //aktualnaMusic =  1
		{
			musicHeroic.stop();
		}
		if(this.aktualnaMusic == 2)  //aktualnaMusic = 2
		{
			musicSad.stop();
		}
		if(this.aktualnaMusic == 3) //aktualnaMusic =  3
		{
			musicWOSP.stop();
		}
		
	}
	public void stopMusic(String nazwa)
	{
		if(nazwa == "musicMenu")
		{
			this.musicMenu.stop();
		}
		if(nazwa == "musicHeroic")
		{
			this.musicHeroic.stop();
		}
		if(nazwa == "musicSad")
		{
			this.musicSad.stop();
		}
		if(nazwa == "musicWOSP")
		{
			this.musicWOSP.stop();
		}
		
	}
	public int zmianaMusic() //wcisniety przycisk N
	{
		System.out.println("bef: "+this.aktualnaMusic);
			if(aktualnaMusic == 0)
				this.aktualnaMusic = 1;
			else if(aktualnaMusic == 3)
				this.aktualnaMusic = 1;
			else
					this.aktualnaMusic++;

			System.out.println("PO: "+this.aktualnaMusic);
		return this.aktualnaMusic;
	}

	public void zatrzymanieMusic()  //wcisniety przycisk M
	{
		
			this.wylaczMusic();
		
	}
	
	
	
}
