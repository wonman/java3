package com.mygdx.game;

import java.util.Date;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.net.Socket;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Text;

public class ProjektJava extends ApplicationAdapter {
	SpriteBatch batch;
	Texture   tMapa2,dtLudek1,dtLudek2,  tLudek1,tLudek2, tMenu, tOautorze, tInfo, tWypis, tBronAK47,tKula;
	private GameObject gracz1, gracz2;
	private OrthographicCamera camera;
	private Game NowaGra;
	private RuchPostaci NowyRuch;
	private Menu NoweMenu;
	private Bronie bronieKula;
	//private BitmapFont font;
	int viewportHeight;
	Date nowaData;
	private Music Music_main;
	private float jumpTime, Timer_gry, shootTime,shootTimeAccept, botTime;
	private Socket socket;
	FreeTypeFontGenerator generator;
	BitmapFont  bounds;
	boolean obrot;
	float Timer_person;
	private Napisy NowyNapis;
	
	
	@Override
	public void create () {	
		obrot = false;
		Music_main = Gdx.audio.newMusic(Gdx.files.internal("Music_main.mp3"));
		Music_main.play();
		camera = new OrthographicCamera(850, 550);
		batch = new SpriteBatch();
		tMapa2 = new Texture("mapa2.png");	
		dtLudek1 = new Texture("dludek1.png");
		dtLudek2 = new Texture("dludek2.png");
		tMenu = new Texture ("menu.png");
		tOautorze = new Texture("oautorze.png");
		tInfo = new Texture("info.png");
		tWypis = new Texture("wypis.png");
		tBronAK47 = new Texture("ak47.png");
		tKula = new Texture("kula.png");
		bronieKula = new Bronie(tKula, 550, 120); ///
		Timer_gry = 0;
		//font = new BitmapFont();
		NowaGra = new Game();
		NowyRuch = new RuchPostaci();
		gracz1 = new GameObject(dtLudek1, 550, 120);
		//gracz1.x = 550;
		//gracz1.y = 120;
		nowaData = new Date();
		gracz2 = new GameObject(dtLudek2, 600, 120);
		//gracz2.x = 600;
		//gracz2.y = 120;
		NoweMenu = new Menu();
		NowyNapis = new Napisy();
		obrot = false;
	
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 1, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game();
	
	}
	
	public void update()
	{
		
		
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		camera.position.set(gracz1.x + gracz1.width/2+100, gracz1.y + gracz1.height/2+250, 0);
		
		if(Gdx.input.isKeyPressed(Keys.SHIFT_LEFT))
		{
			camera.zoom+=0.02;
			System.out.println("zoom: "+camera.zoom);
		}
		if(Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT))
			camera.zoom-=0.02;
		
		jumpTime += Gdx.graphics.getDeltaTime();
		if( (Gdx.input.isKeyPressed(Keys.W) && jumpTime>0.1) || NowyRuch.getWarunekSkoku()  ) //gora
		{
			NowyRuch.setTrue();
			System.out.println("wysokosc "+gracz1.y+ ", wartosc "+NowyRuch.getWarunekSkoku());
			float skok = -25*jumpTime * jumpTime + 25* jumpTime;
			gracz1.y += skok;
			if(gracz1.y<120)
			{
				NowyRuch.setFalse();
				gracz1.y=120;
				jumpTime =0;
			}
				
		}
		

		if(Gdx.input.isKeyPressed(Keys.A)&& gracz1.x>600) //lewo ruch	
			gracz1.x -=10;
		
		if(Gdx.input.isKeyPressed(Keys.D)&& gracz1.x<2900) //prawo ruch
			gracz1.x +=10;
		
		shootTimeAccept += Gdx.graphics.getDeltaTime();
		shootTime += Gdx.graphics.getDeltaTime();
		if( Gdx.input.isKeyPressed(Keys.CONTROL_LEFT) && shootTimeAccept>1.5  ) //strzal
		{
			bronieKula.strza�Accept();
			bronieKula.setShotTrue();
			shootTimeAccept = 0;
		}
		if(bronieKula.getShot() == true && shootTime > 0.2)
		{
			bronieKula.strza�();
		}
		
		botTime += Gdx.graphics.getDeltaTime();
		if(botTime > 0.1)								//ruch bota
		{
			if(gracz2.naprzodwPrawo)
			{
				gracz2.x += 2;
				if(gracz2.x >3000)
					gracz2.naprzodwPrawo = false;
			}
			else
			{
				gracz2.x -= 2;
				if(gracz2.x <300)
					gracz2.naprzodwPrawo = true;
			}
			
			
		}
		
	}
	public void draw()
	{
		
		batch.draw(tMapa2, 0, 0);
		Timer_person+= Gdx.graphics.getDeltaTime();
		if(Timer_person> 5 )
		{
		  if(obrot == false)
			  obrot = true;
		  else
			  obrot = false;
		  System.out.println(obrot);
		  Timer_person = 0;
		}
		batch.draw(gracz1.getTexture(), gracz1.x, gracz1.y, 155, 325, 0, 0,  155, 325, obrot, false); //gracz 1 ruch
		batch.draw(tBronAK47, gracz1.x, gracz1.y+140, 191, 61, 0, 0,  191, 61, obrot, false); // bron AK47 ruch
		batch.draw(gracz2.getTexture(), gracz2.x, gracz2.y); // gracz 2 ruch
		
		Timer_gry += Gdx.graphics.getDeltaTime();
		String napisik = Float.toString(Timer_gry); 
		NowyNapis.get_BitmapFontGracz1().draw(batch, NowyNapis.get_stringNapisGracz1() ,gracz1.x+45, gracz1.y+370); //gracz1 napis nad g�ow�
		NowyNapis.get_BitmapFontGracz2().draw(batch, NowyNapis.get_stringNapisGracz2(),gracz2.x+45, gracz2.y+370); //gracz1 napis nad g�ow�
		NowyNapis.get_BitmapFontGracz1().draw(batch, "TIME   "+napisik, gracz1.x+400, gracz1.y+570); //napis czasu
		
		if(bronieKula.getShot())
		{
			if(obrot == false)
			batch.draw(tKula, gracz1.x +50 + bronieKula.strza�(), gracz1.y+185); 
			if(obrot == true)
			batch.draw(tKula, gracz1.x +50 + bronieKula.strza�()*(-1), gracz1.y+185); 
		}
		
	}
	
	public void game()
	{
		if(NowaGra.getStanGry() == -1) //zapis daty do pliku
		{
			NoweMenu.Opcja0();
			NowaGra.setStanGry(0);
		}
		batch.begin();
		Gdx.gl.glClearColor(1, 1, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);	// czyszczenie
		
		if(NowaGra.getStanGry() == 0) //nie gra nikt
		{
			camera.zoom= 1f;
			batch.setProjectionMatrix(camera.combined);
			camera.position.set(425, 275, 0);
			camera.update();
			batch.draw(tMenu, 0, 0);
			NowaGra.setStanGry( NoweMenu.Opcja1() );
			if(Gdx.input.isKeyPressed(Keys.TAB)) Gdx.app.exit(); //wyjscie TAB
		}
		
		else if(NowaGra.getStanGry() == 1) //preGra
		{
			camera.zoom= 1.33f;
			NowaGra.setStanGry( 2 );
		}
		else if(NowaGra.getStanGry() == 2)
		{
			draw();
			update();
			NowaGra.setStanGry( NoweMenu.ESC(2) );
		}
		else if(NowaGra.getStanGry() == 3) // wybrano info
		{
			batch.draw(tInfo, 0, 0);
			//font.draw(batch, "HELLO", 300, 300);
			NowaGra.setStanGry( NoweMenu.ESC(3) );
		}
		else if(NowaGra.getStanGry() == 4) // wybrano oautorach
		{
			batch.draw(tOautorze, 0, 0);		
			NowaGra.setStanGry( NoweMenu.ESC(4) );
		}
		else if(NowaGra.getStanGry() == 5) // wizyty
		{
			batch.draw(tWypis, 0, 0);
			FileHandle file = Gdx.files.internal("data.txt");
			String text = file.readString();
			file.exists();
			NoweMenu.get_BitmapFont_TekstMenu().draw(batch, text , 177, 380); //wypis wpis�w HIGHSCORES
			NoweMenu.get_BitmapFont_Tytu�Menu().draw(batch, NoweMenu.get_String_Tytu�Hightscores() , 140, 500); //wypis tytu�"HIGHSCORES"
					
			NowaGra.setStanGry( NoweMenu.ESC(5) );
			if(Gdx.input.isKeyPressed(Keys.BACKSPACE)) //usuwani wpis�w
			{
				FileHandle file1 = Gdx.files.local("data.txt");
				file1.writeString("" , false);
				System.out.println("reset");
			}
		}
		batch.end(); //--
	}
	
	
	@Override
	public void dispose() 
	{
		System.out.print("wyj�cie");
		super.dispose(); 
		tMapa2.dispose(); dtLudek1.dispose(); dtLudek2.dispose();   tLudek1.dispose(); tLudek2.dispose();  tMenu.dispose();  tOautorze.dispose();  tInfo.dispose();  tWypis.dispose(); 
		batch.dispose(); 
		Music_main.dispose();
		generator.dispose();
	}
}
